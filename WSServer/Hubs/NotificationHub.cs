﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace WSServer.Hubs
{
    public class NotificationHub : Hub
    {
        public ILogger<NotificationHub> Logger {get;}

        public NotificationHub(ILogger<NotificationHub> logger)
        {
            Logger = logger;
        }

        public async Task SendMessageToAllUsers(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }
        public Task SendMessageToCaller(string message)
        {
            return Clients.Caller.SendAsync("ReceiveMessage", message);
        }
        public async Task SendMessageToClient(string clientId, string message)
        {
            await Clients.Client(clientId).SendAsync("ReceiveMessage", message);
        }
        public async Task SendMessageToGroup(string group, string message)
        {
            await Clients.Group(group).SendAsync("ReceiveMessage", message);
        }
        public async Task JoinGroup(string group)
        {
            await Groups.AddToGroupAsync(group, Context.ConnectionId);
        }

        public override async Task OnConnectedAsync()
        {
            Logger.LogInformation($"New user {Context.User.Identity.Name}({Context.ConnectionId}) connected.");
            await base.OnConnectedAsync();
        }
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            Logger.LogInformation($"The user {Context.User.Identity.Name}({Context.ConnectionId}) disconnected.");
            await base.OnDisconnectedAsync(exception);
        }
    }
}
