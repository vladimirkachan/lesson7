﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace WSClient
{
    class Program
    {
        static HubConnection connection;

        static async Task Main(string[] args)
        {
            connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/notification")
                .WithAutomaticReconnect()
                .Build();

            connection.On<string>("ReceiveMessage", message =>
            {
                var m = $"{DateTime.Now.ToShortTimeString()}: {message}";
                Console.WriteLine(m);
            });

            try
            {
                await connection.StartAsync();
                Console.WriteLine("Connection started");
                await WaitForMessages();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static async Task WaitForMessages()
        {
            while (true)
            {
                try
                {
                    Console.Write("You say: ");
                    var message = Console.ReadLine();
                    await connection.InvokeAsync("SendMessageToAllUsers", message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
